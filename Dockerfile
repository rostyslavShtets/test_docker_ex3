FROM golang
 
ADD . /go/src/go_docker_ex3
RUN go install go_docker_ex3
ENTRYPOINT /go/bin/basic_web_server
 
EXPOSE 8080

FROM golang:onbuild
EXPOSE 8080